#include <string>
#include <vector>

using namespace std;

int solution(string s)
{
    int answer = 0;
    int stringSize = s.size();
    int temanswer = stringSize;
    int zipStrSize = 0;
    int divideNum = 1;
    while(divideNum <= stringSize/2)//나누는 수
    {
        
        if(stringSize % divideNum == 0)//압축하기
        {
            int BaseFlg = 0;
            int comFlg;
                       
            while(BaseFlg <= stringSize - divideNum)//값 계산
            {
                int count = 0;//여기에 문자열 길이 압축하며 더해가기
                comFlg = BaseFlg + divideNum;
               
                while(comFlg <= stringSize - divideNum)
                {
                    
                    bool Istrue = true;
                    
                    for(int i = 0; i < divideNum; i++)
                    {
                        if(s[BaseFlg + i] != s[comFlg + i])
                        {
                            Istrue = false;
                        }
                    }
                     
                    if(Istrue == true)
                    {
                        comFlg = comFlg + divideNum;
                        count++;
                    }
                    else// 다르다
                    {
                        break;
                    }
                    
                 }
                  if(count != 0)
                  {
                      string temS = to_string(count);  
                      zipStrSize += temS.size() + divideNum;  
                  }
                else
                {
                    zipStrSize += 1;
                }
                                          
                BaseFlg = comFlg;
    
            }
            
            divideNum++;
         }
        
        else //압축 불가능
        {
            divideNum++;
        }
        
        if(zipStrSize != 0 && zipStrSize < temanswer)
        {
            temanswer = zipStrSize;
        }
        
    }
    
    answer = temanswer;
    
    return answer;
}